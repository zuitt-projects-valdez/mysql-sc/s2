-- allow us to import and export whole sql commands but for this case - for notes 

-- command: list all databases; usually done in terminal bc easier 
SHOW DATABASES;

-- command: create a database; usually all lowercase 
CREATE DATABASE music_db; 

-- command: drop/delete a database; should be used cautiously 
DROP DATABASE music_db; 

-- select/use a db; can't directly start typing commands phpmyadmin needs to know which db to use 
USE music_db; 

-- create tables 
-- table columns have the following format: [column_name] [date_type] [other_options]
CREATE TABLE users (
  id INT NOT NULL AUTO_INCREMENT, 
  username VARCHAR(50) NOT NULL, 
  password VARCHAR(50) NOT NULL, 
  full_name VARCHAR(50) NOT NULL, 
  contact_number INT NOT NULL, 
  email VARCHAR(50), 
  address VARCHAR(50),  
  PRIMARY KEY(id) --have to define primary key which is almost always id (always want to have the first and last line in every table)
);

-- drop/delete table 
DROP TABLE users; --or whatever the table name is 

-- create the artist table
CREATE TABLE artists (
 id INT NOT NULL AUTO_INCREMENT, 
  name VARCHAR(50) NOT NULL, 
  PRIMARY KEY(id) 
);

--create the albums table which references the artists table 
CREATE TABLE albums (
  id iNT NOT NULL AUTO_INCREMENT, 
  album_title VARCHAR(50) NOT NULL, 
  date_released DATE NOT NULL, 
  artist_id INT NOT NULL, 
  PRIMARY KEY (id), 
  CONSTRAINT fk_albums_artist_id
    FOREIGN KEY (artist_id) REFERENCES artists(id)
    ON UPDATE CASCADE -- if there are any changes to the id (name or value) of the artist in the artist table, this command will then cascade from parent to child the update and change the artist_id in the albums table 
    ON DELETE RESTRICT -- will prevent you from deleting an artist from the artists table, will have to delete the album first then the artist  
); 

-- create the songs table which references the albums table 
CREATE TABLE songs(
  id INT NOT NULL AUTO_INCREMENT, 
  song_name VARCHAR(50) NOT NULL, 
  length TIME NOT NULL, 
  genre VARCHAR(50) NOT NULL, 
  album_id INT NOT NULL, 
  PRIMARY KEY (id), 
  CONSTRAINT fk_songs_album_id
    FOREIGN KEY(album_id) REFERENCES albums(id)
    ON UPDATE CASCADE
    ON DELETE RESTRICT
);

-- playlist table which references users 
  CREATE TABLE playlists (
    id INT NOT NULL AUTO_INCREMENT, 
    user_id INT NOT NULL,
    PRIMARY KEY (id), 
    CONSTRAINT fk_playlists_user_id
      FOREIGN KEY (user_id) REFERENCES users(id)
      ON UPDATE CASCADE
      ON DELETE RESTRICT
  )

-- create playlist_songs linking table which links playlist table with our songs table 
CREATE TABLE playlists_songs (
  id INT NOT NULL AUTO_INCREMENT, 
  playlist_id INT NOT NULL, 
  song_id INT NOT NULL, 
  PRIMARY KEY (id), 
  CONSTRAINT fk_playlists_songs_playlist_id
    FOREIGN KEY(playlist_id) REFERENCES playlists(id)
    ON UPDATE CASCADE
    ON DELETE RESTRICT, 
  CONSTRAINT fk_playlists_songs_song_id
    FOREIGN KEY(song_id) REFERENCES songs(id)
    ON UPDATE CASCADE
    ON DELETE RESTRICT,   
);

-- sql is the preferred database for any business that needs rigid structure rather than mongodb which is more freeform; businesses like banks need more strict and rigid structures in their db 
-- ecommerce apps - mongodb can have the same amount of structure and consistency as mysql; very scalable; more work because it doesnt start that way but can make it work just as well; mysql is easier to start with bec the structure is there by default with the rigid tables; mysql is older that's why most still use it 